package lab.stellar.boot.dao;

import lab.stellar.boot.model.PlanetarySystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SystemDAO extends JpaRepository<PlanetarySystem, Integer> {

    //@Query("select ps from PlanetarySystem ps where ps.name like :phrase")
    List<PlanetarySystem> findAllByNameContainingIgnoreCase(String phrase);
}
