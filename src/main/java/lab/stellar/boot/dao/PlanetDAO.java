package lab.stellar.boot.dao;

import lab.stellar.boot.model.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetDAO extends JpaRepository<Planet, Integer> {
}
