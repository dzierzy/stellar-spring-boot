package lab.stellar.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StellarStarter {

    public static void main(String[] args) {
        SpringApplication.run(StellarStarter.class, args);
    }
}
