package lab.stellar.boot.web;

import lab.stellar.boot.dao.PlanetDAO;
import lab.stellar.boot.dao.SystemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/web")
public class SystemsController {

    @Autowired
    SystemDAO systemDao;

    @Autowired
    PlanetDAO planetDAO;

    @GetMapping("/systems")
    public String getSystems(Model model,@RequestParam(value="phrase", required = false) String phrase){
        if(phrase==null) {
            model.addAttribute("systems", systemDao.findAll());
        } else {
            model.addAttribute("systems", systemDao.findAllByNameContainingIgnoreCase(phrase));
        }
        return "systems";
    }
}
